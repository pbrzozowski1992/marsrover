package gov.nasa.marsrover.ui.view.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;

import gov.nasa.marsrover.R;
import gov.nasa.marsrover.ui.view.fragment.RoverPhotosFragment;

public class MainActivity extends BaseActivity {

    private FragmentWrapper fragmentWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void addContent(FragmentTransaction transaction) {
        fragmentWrapper = new RoverPhotosFragmentWrapper(transaction);
    }

    private class RoverPhotosFragmentWrapper implements FragmentWrapper {

        RoverPhotosFragmentWrapper(FragmentTransaction transaction) {
            RoverPhotosFragment roverPhotosFragment = new RoverPhotosFragment();
            transaction.add(R.id.rover_photos_container, roverPhotosFragment);
        }

        @Override
        public void onBackPressed() {

        }
    }
}
