package gov.nasa.marsrover.ui.viewmodel;


import gov.nasa.marsrover.core.model.Photo;

public class RoverPhotoViewModel extends ItemViewModel<Photo> {

    private final ViewListener viewListener;
    private Photo photo;

    public interface ViewListener {
        void displayPhoto(String url);
    }

    public RoverPhotoViewModel(Photo photo, ViewListener listener) {
        this.photo = photo;
        this.viewListener = listener;
    }

    @Override
    public void destroy() {

    }

    @Override
    public void setItem(Photo item) {
        this.photo = item;
    }

    public String getId() {
        return Long.toString(photo.getId());
    }

    public String getEarthDate() {
        return photo.getEarthDate();
    }

    public String getRover() {
        return photo.getRover().toString();
    }

    public String getCamera() {
        return photo.getCamera().toString();
    }

    public void onRoverPhotoItemClick() {
        viewListener.displayPhoto(photo.getImgSrc());
    }
}
