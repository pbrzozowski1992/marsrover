package gov.nasa.marsrover.ui.viewmodel;


import java.util.List;

import gov.nasa.marsrover.core.model.Photo;
import gov.nasa.marsrover.core.repository.PhotosRepository;
import gov.nasa.marsrover.utils.Log;

public class RoverPhotosViewModel implements ViewModel{

    private static final String TAG = RoverPhotosViewModel.class.getSimpleName();

    private final ViewListener viewListener;
    private final PhotosRepository photosRepository;

    public interface ViewListener {
        void onPhotosReceived(List<Photo> photos);
    }

    public RoverPhotosViewModel(PhotosRepository photosRepository, ViewListener viewListener) {
        this.viewListener = viewListener;
        this.photosRepository = photosRepository;
        init();
    }

    @Override
    public void destroy() {

    }

    private void init() {
        photosRepository.fetchPhotos(new PhotosRepository.OnPhotosReceiveListener() {
            @Override
            public void onPhotosReceived(List<Photo> photos) {
                Log.d(TAG, "photos received: " + photos.size());
                viewListener.onPhotosReceived(photos);
            }
        });
    }
}
