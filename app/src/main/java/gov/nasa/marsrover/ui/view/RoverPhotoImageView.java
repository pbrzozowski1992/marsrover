package gov.nasa.marsrover.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import java.io.InputStream;
import java.util.List;

import gov.nasa.marsrover.AndroidHandlerWrapper;
import gov.nasa.marsrover.core.ActionExecutor;
import gov.nasa.marsrover.core.HttpActionExecutor;
import gov.nasa.marsrover.core.action.Action;
import gov.nasa.marsrover.core.action.GetMarsPhoto;
import gov.nasa.marsrover.utils.Log;


public class RoverPhotoImageView extends android.support.v7.widget.AppCompatImageView {

    private static final String TAG = RoverPhotoImageView.class.getSimpleName();

    public RoverPhotoImageView(Context context) {
        super(context);
        init();
    }

    public RoverPhotoImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RoverPhotoImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        //TODO: implement init
    }

    public void bind(String url) {
        ActionExecutor actionExecutor = new HttpActionExecutor(new AndroidHandlerWrapper(new Handler()));
        Log.d(TAG, "bind url: " + url);
        final GetMarsPhoto action = GetMarsPhoto.create(url);
        action.setOnProcessListener(new Action.OnProcessListener<InputStream>() {
            @Override
            public void onProcessFinished(List<InputStream> elements) {
                displayImage(action, elements.get(0));
            }
        });
        actionExecutor.submit(action);
    }

    private void displayImage(Action<InputStream> action, InputStream inputStream) {
        new DisplayImageAsyncTask(action).execute(inputStream);
    }

    private class DisplayImageAsyncTask extends AsyncTask<InputStream,Void,Bitmap>{

        private Action<InputStream> action;

        DisplayImageAsyncTask(Action<InputStream> action) {
            this.action = action;
        }

        @Override
        protected Bitmap doInBackground(InputStream... params) {
            return BitmapFactory.decodeStream(params[0]);
        }

        @Override
        protected void onPostExecute(Bitmap aBitmap) {
            super.onPostExecute(aBitmap);
            RoverPhotoImageView.this.setImageBitmap(aBitmap);
            action.complete();
        }
    }
}
