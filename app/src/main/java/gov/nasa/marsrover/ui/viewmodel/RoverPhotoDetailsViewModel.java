package gov.nasa.marsrover.ui.viewmodel;


public class RoverPhotoDetailsViewModel implements ViewModel {

    private String imageUrl;

    public RoverPhotoDetailsViewModel(String url) {
        this.imageUrl = url;
    }

    public String getImageUrl(){
        return imageUrl;
    }

    @Override
    public void destroy() {

    }
}
