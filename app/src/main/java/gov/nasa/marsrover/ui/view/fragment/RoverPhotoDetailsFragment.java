package gov.nasa.marsrover.ui.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gov.nasa.marsrover.R;
import gov.nasa.marsrover.databinding.FragmentRoverPhotoDetailsBinding;
import gov.nasa.marsrover.ui.viewmodel.RoverPhotoDetailsViewModel;

public class RoverPhotoDetailsFragment extends BaseFragment {

    public static final String URL_KEY = "url_key";
    private FragmentRoverPhotoDetailsBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rover_photo_details, container, false);
        binding.setViewModel(new RoverPhotoDetailsViewModel(args.getString(URL_KEY)));
        return binding.getRoot();
    }
}
