package gov.nasa.marsrover.ui.bindings;


import android.databinding.BindingAdapter;

import gov.nasa.marsrover.ui.view.RoverPhotoImageView;

public class ImageViewBindingAdapter {

    @BindingAdapter({"android:src"})
    public static void setImageResource(RoverPhotoImageView imageView, String url) {
        imageView.bind(url);
    }
}
