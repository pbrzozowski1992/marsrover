package gov.nasa.marsrover.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import gov.nasa.marsrover.R;
import gov.nasa.marsrover.core.model.Photo;
import gov.nasa.marsrover.databinding.RoverPhotoItemBinding;
import gov.nasa.marsrover.ui.viewmodel.RoverPhotoViewModel;
import gov.nasa.marsrover.utils.Log;


public class RoverPhotosAdapter extends RecyclerView.Adapter<RoverPhotosAdapter.RoverPhotosViewHolder> {

    private static final String TAG = RoverPhotosAdapter.class.getSimpleName();
    private List<Photo> photos = new ArrayList<>();
    private final RoverPhotoViewModel.ViewListener viewListener;

    public RoverPhotosAdapter(RoverPhotoViewModel.ViewListener listener) {
        this.viewListener = listener;
    }

    @Override
    public RoverPhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RoverPhotoItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.rover_photo_item,
                parent,
                false);
        return new RoverPhotosViewHolder(binding, viewListener);
    }

    @Override
    public void onBindViewHolder(RoverPhotosViewHolder holder, int position) {
        holder.bind(photos.get(position));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    static class RoverPhotosViewHolder extends RecyclerView.ViewHolder {
        final RoverPhotoItemBinding binding;
        final RoverPhotoViewModel.ViewListener viewListener;

        RoverPhotosViewHolder(RoverPhotoItemBinding binding, RoverPhotoViewModel.ViewListener listener) {
            super(binding.cardView);
            this.binding = binding;
            this.viewListener = listener;
        }

        void bind(Photo photo) {
            Log.d(TAG, "photo to bind: " + photo.toString());
            if (binding.getViewModel() == null) {
                binding.setViewModel(new RoverPhotoViewModel(photo, viewListener));
            } else {
                binding.getViewModel().setItem(photo);
            }
        }
    }
}
