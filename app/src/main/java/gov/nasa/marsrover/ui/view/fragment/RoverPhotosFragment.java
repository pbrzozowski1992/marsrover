package gov.nasa.marsrover.ui.view.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import gov.nasa.marsrover.AndroidHandlerWrapper;
import gov.nasa.marsrover.R;
import gov.nasa.marsrover.core.ActionExecutor;
import gov.nasa.marsrover.core.HttpActionExecutor;
import gov.nasa.marsrover.core.model.Photo;
import gov.nasa.marsrover.core.repository.RoverPhotosRepository;
import gov.nasa.marsrover.databinding.FragmentRoverPhotosBinding;
import gov.nasa.marsrover.ui.adapter.RoverPhotosAdapter;
import gov.nasa.marsrover.ui.viewmodel.RoverPhotoViewModel;
import gov.nasa.marsrover.ui.viewmodel.RoverPhotosViewModel;

public class RoverPhotosFragment extends BaseFragment{

    private FragmentRoverPhotosBinding binding;
    private RoverPhotosViewModel.ViewListener roverPhotosViewListener = new RoverPhotosViewModel.ViewListener() {
        @Override
        public void onPhotosReceived(List<Photo> photos) {
            RoverPhotosAdapter adapter = (RoverPhotosAdapter) binding.recyclerView.getAdapter();
            adapter.setPhotos(photos);
            adapter.notifyDataSetChanged();
        }
    };

    private RoverPhotoViewModel.ViewListener roverPhotoViewListener = new RoverPhotoViewModel.ViewListener() {
        @Override
        public void displayPhoto(String url) {
            Bundle args = new Bundle();
            args.putString(RoverPhotoDetailsFragment.URL_KEY, url);
            RoverPhotoDetailsFragment fragment = new RoverPhotoDetailsFragment();
            fragment.setArguments(args);
            getFragmentManager().beginTransaction().add(R.id.rover_photos_container,
                    fragment).commit();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        ActionExecutor actionExecutor = new HttpActionExecutor(new AndroidHandlerWrapper(new Handler()));
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rover_photos, container, false);
        binding.setViewModel(new RoverPhotosViewModel(new RoverPhotosRepository(actionExecutor), roverPhotosViewListener));
        setUpRecyclerView(binding.recyclerView);
        return binding.getRoot();
    }

    private void setUpRecyclerView(RecyclerView recyclerView) {
        RoverPhotosAdapter adapter = new RoverPhotosAdapter(roverPhotoViewListener);
        recyclerView.setAdapter(adapter);
    }
}
