package gov.nasa.marsrover.ui.view.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpFragments();
    }

    private void setUpFragments() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        addContent(transaction);
        transaction.commit();
    }

    protected abstract void addContent(FragmentTransaction transaction);

    protected interface FragmentWrapper {
        void onBackPressed();
    }
}
