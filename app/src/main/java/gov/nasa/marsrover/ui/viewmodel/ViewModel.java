package gov.nasa.marsrover.ui.viewmodel;


public interface ViewModel {

    void destroy();
}
