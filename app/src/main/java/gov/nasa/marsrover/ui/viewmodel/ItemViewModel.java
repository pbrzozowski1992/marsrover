package gov.nasa.marsrover.ui.viewmodel;


public abstract class ItemViewModel<T> implements ViewModel {

    public abstract void setItem(T item);
}
