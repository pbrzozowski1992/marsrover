package gov.nasa.marsrover;

import android.os.Handler;

import gov.nasa.marsrover.core.external.HandlerWrapper;


public class AndroidHandlerWrapper implements HandlerWrapper {

    private final Handler handler;

    public AndroidHandlerWrapper(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void post(Runnable runnable) {
        handler.post(runnable);
    }
}
