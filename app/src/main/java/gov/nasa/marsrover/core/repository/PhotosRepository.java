package gov.nasa.marsrover.core.repository;


import java.util.List;

import gov.nasa.marsrover.core.model.Photo;

public interface PhotosRepository {

    interface OnPhotosReceiveListener {
        void onPhotosReceived(List<Photo> photos);
    }

    void fetchPhotos(OnPhotosReceiveListener listener);
    void setPhotos(List<Photo> photos);
}
