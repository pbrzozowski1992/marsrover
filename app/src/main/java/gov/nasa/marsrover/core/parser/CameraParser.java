package gov.nasa.marsrover.core.parser;

import org.json.JSONException;
import org.json.JSONObject;

import gov.nasa.marsrover.core.model.Camera;


public class CameraParser extends BaseParser<Camera> {
    @Override
    public Camera parseFromJsonObject(JSONObject jsonObject) throws JSONException {
        long id = jsonObject.optLong(Camera.ID);
        String name = jsonObject.optString(Camera.NAME);
        long roverId = jsonObject.optLong(Camera.ROVER_ID);
        String fullName = jsonObject.optString(Camera.FULL_NAME);
        return new Camera(id, name, roverId, fullName);
    }
}
