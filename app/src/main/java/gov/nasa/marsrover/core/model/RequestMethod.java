package gov.nasa.marsrover.core.model;


public enum RequestMethod {

    GET("GET"),
    POST("POST"),
    DELETE("DELETE");

    private String requestMethod;
    RequestMethod(String methodType){
        this.requestMethod = methodType;
    }

    public String getRequestMethod() {
        return requestMethod;
    }
}
