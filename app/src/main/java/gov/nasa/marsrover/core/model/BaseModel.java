package gov.nasa.marsrover.core.model;


public abstract class BaseModel {

    public static String ID = "id";

    private long id = DefaultValues.DEFAULT_ID;

    public BaseModel(long id){
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
