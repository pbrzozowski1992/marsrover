package gov.nasa.marsrover.core.parser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import gov.nasa.marsrover.core.model.Camera;
import gov.nasa.marsrover.core.model.Rover;


public class RoverParser extends BaseParser<Rover> {
    @Override
    public Rover parseFromJsonObject(JSONObject jsonObject) throws JSONException {
        CameraParser cameraParser = new CameraParser();
        long id = jsonObject.optLong(Rover.ID);
        String name = jsonObject.optString(Rover.NAME);
        String landingDate = jsonObject.optString(Rover.LANDING_DATE);
        String launchDate = jsonObject.optString(Rover.LAUNCH_DATE);
        String status = jsonObject.optString(Rover.STATUS);
        int maxSol = jsonObject.optInt(Rover.MAX_SOL);
        String maxDate = jsonObject.optString(Rover.MAX_DATE);
        int totalPhotos = jsonObject.optInt(Rover.TOTAL_PHOTOS);
        List<Camera> cameras = cameraParser.parseFromJsonArray(jsonObject.optJSONArray(Rover.CAMERAS));
        return new Rover(id, name, landingDate, launchDate, status, maxSol, maxDate, totalPhotos, cameras);
    }
}
