package gov.nasa.marsrover.core.action;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import gov.nasa.marsrover.core.CoreException;
import gov.nasa.marsrover.core.model.RequestMethod;

public interface Action<T> {
    int DEFAULT_CONNECTION_TIMEOUT = 10000;

    interface OnExecuteListener {
        void onExecuteSuccess(Action action);
        void onExecuteError(Action action, CoreException e);
    }

    interface OnProcessListener<T> {
        void onProcessFinished(List<T> elements);
    }

    void onSubmit();
    void onExecute();
    void onPostExecute();
    void onProcessResponse(String response);
    void setOnExecuteListener(OnExecuteListener listener);
    void setOnProcessListener(OnProcessListener<T> listener);
    void complete();
    RequestMethod getMethod();
    URL getUrl() throws MalformedURLException;
    int getConnectionTimeout();
}
