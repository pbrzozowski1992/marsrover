package gov.nasa.marsrover.core;


import gov.nasa.marsrover.core.action.Action;

public interface ActionExecutor {
    void submit(Action action);
    void removePendingAction();
}
