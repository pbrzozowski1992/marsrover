package gov.nasa.marsrover.core.parser;

import org.json.JSONException;
import org.json.JSONObject;

import gov.nasa.marsrover.core.model.Camera;
import gov.nasa.marsrover.core.model.Photo;
import gov.nasa.marsrover.core.model.Rover;


public class PhotoParser extends BaseParser<Photo> {

    @Override
    public Photo parseFromJsonObject(JSONObject jsonObject) throws JSONException {
        CameraParser cameraParser = new CameraParser();
        RoverParser roverParser = new RoverParser();
        long id = jsonObject.optLong(Photo.ID);
        long sol = jsonObject.optLong(Photo.SOL);
        String imgSrc = jsonObject.optString(Photo.IMAGE_SRC);
        String earthDate = jsonObject.optString(Photo.EARTH_DATE);
        Camera camera = cameraParser.parseFromJsonObject(jsonObject.optJSONObject(Photo.CAMERA));
        Rover rover = roverParser.parseFromJsonObject(jsonObject.optJSONObject(Photo.ROVER));
        return new Photo(id, sol, imgSrc, earthDate, camera, rover);
    }
}
