package gov.nasa.marsrover.core;


public class CoreException extends RuntimeException {

    private ErrorCode errorCode;
    private Throwable originalThrowable;

    public enum ErrorCode {
        GENERAL_ERROR,
        HTTP_URL_CONNECTION_ERROR,
        INVALID_RESPONSE
    }

    public CoreException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public CoreException(ErrorCode errorCode, Throwable originalThrowable) {
        if (errorCode == null) throw new NullPointerException("error code cannot be null!");
        this.errorCode = errorCode;
        this.originalThrowable = originalThrowable;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public Throwable getOriginalThrowable() {
        return originalThrowable;
    }
}
