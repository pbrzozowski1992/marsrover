package gov.nasa.marsrover.core.action;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import gov.nasa.marsrover.core.CoreException;
import gov.nasa.marsrover.utils.Log;

public abstract class BaseImageAction extends BaseHttpAction<InputStream>{

    private static final String TAG = BaseImageAction.class.getSimpleName();

    private HttpURLConnection httpURLConnection;

    public BaseImageAction(String uri) {
        super(uri, null);
    }

    @Override
    public void onPostExecute() {
        Log.d(TAG, "onPostExecute");
        notifyOnProcessFinished();
    }

    @Override
    public void onProcessResponse(String response) {
    }

    @Override
    protected void executeAction() throws IOException {
        httpURLConnection = buildHttpURLConnection();
        httpURLConnection.connect();
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            notifyOnExecutionError(new CoreException(CoreException.ErrorCode.HTTP_URL_CONNECTION_ERROR));
        }
        elements.add(httpURLConnection.getInputStream());
        notifyOnExecutionSuccess();
    }

    @Override
    public void complete() {
        try {
            if (httpURLConnection != null) {
                httpURLConnection.getInputStream().close();
                httpURLConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected HttpURLConnection buildHttpURLConnection() throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) getUrl().openConnection();
        httpURLConnection.setConnectTimeout(getConnectionTimeout());
        return httpURLConnection;
    }
}
