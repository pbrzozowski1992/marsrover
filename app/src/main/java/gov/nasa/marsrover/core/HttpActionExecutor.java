package gov.nasa.marsrover.core;

import android.support.annotation.NonNull;

import java.util.ArrayDeque;
import java.util.Queue;


import gov.nasa.marsrover.core.action.Action;
import gov.nasa.marsrover.core.external.HandlerWrapper;
import gov.nasa.marsrover.utils.Log;


public class HttpActionExecutor implements ActionExecutor {

    private static final String TAG = HttpActionExecutor.class.getSimpleName();

    private final Queue<Action> actionQueue;
    private final HandlerWrapper handlerWrapper;
    private Thread actionThread;

    public HttpActionExecutor(@NonNull HandlerWrapper handlerWrapper) {
        this.actionQueue = new ArrayDeque<>();
        this.handlerWrapper = handlerWrapper;
        startActionThread();
    }

    @Override
    public void submit(Action action) {
        synchronized (actionQueue) {
            actionQueue.offer(action);
            actionQueue.notifyAll();
            Log.d(TAG, String.format("submit: %s, queue size: %s", action, actionQueue.size()));
        }
    }

    @Override
    public void removePendingAction() {
        synchronized (actionQueue) {
            for (Action action : actionQueue) {
                Log.d(TAG, "action will not be executed: " + action);
                //action error
            }
            actionQueue.clear();
        }
    }

    private void startActionThread() {
        actionThread = new ActionThread();
        actionThread.start();
    }

    private class ActionThread extends Thread {
        @Override
        public void run() {
            while (true) {
                super.run();
                if (isInterrupted()) {
                    Log.d(TAG, "ActionThread interrupted!");
                    removePendingAction();
                    return;
                }

                try {
                    Action actionToExecute = waitForAction();
                    onExecute(actionToExecute);
                    onPostExecute(actionToExecute);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void onExecute(Action action) {
        Log.d(TAG, "onExecute: " + action);
        action.onExecute();
    }

    private void onPostExecute(final Action action) {
        Log.d(TAG, "onPostExecute: " + action);
        handlerWrapper.post(new Runnable() {
            @Override
            public void run() {
                action.onPostExecute();
            }
        });
    }

    private Action waitForAction() throws InterruptedException {
        Log.d(TAG, "waitForAction");
        Action actionToExecute;
        while (true) {
            synchronized (actionQueue) {
                actionToExecute = actionQueue.poll();
                if (actionToExecute != null) {
                    return actionToExecute;
                } else {
                    actionQueue.wait();
                }
            }
        }
    }

}
