package gov.nasa.marsrover.core.parser;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public interface IParser<T> {

    List<T> parseFromJsonArray(JSONArray jsonArray) throws JSONException;
    T parseFromJsonObject(JSONObject jsonObject) throws JSONException;
}
