package gov.nasa.marsrover.core.parser;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseParser<T> implements IParser<T>{

    @Override
    public List<T> parseFromJsonArray(JSONArray jsonArray) throws JSONException {
        List<T> elements = new ArrayList<>();
        for(int arrayIndex = 0; arrayIndex < jsonArray.length(); arrayIndex++) {
            JSONObject jsonObject = jsonArray.getJSONObject(arrayIndex);
            elements.add(parseFromJsonObject(jsonObject));
        }
        return elements;
    }
}
