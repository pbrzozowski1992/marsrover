package gov.nasa.marsrover.core.action;


import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import gov.nasa.marsrover.core.model.RequestMethod;
import gov.nasa.marsrover.utils.Log;

public class GetMarsPhoto extends BaseImageAction{

    private static final String TAG = GetMarsPhotos.class.getSimpleName();

    public static GetMarsPhoto create(String uri) {
        return new GetMarsPhoto(uri);
    }

    private GetMarsPhoto(String uri) {
        super(uri);
    }

    @Override
    public void onSubmit() {
        Log.d(TAG, "onSubmit");
    }

    @Override
    public RequestMethod getMethod() {
        return null;
    }

    @Override
    public URL getUrl() throws MalformedURLException {
        return URI.create(uri).toURL();
    }
}
