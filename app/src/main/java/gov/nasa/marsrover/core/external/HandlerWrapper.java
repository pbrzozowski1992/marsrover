package gov.nasa.marsrover.core.external;


public interface HandlerWrapper {

    void post(Runnable runnable);
}
