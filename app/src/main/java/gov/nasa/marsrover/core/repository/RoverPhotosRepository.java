package gov.nasa.marsrover.core.repository;


import java.util.ArrayList;
import java.util.List;

import gov.nasa.marsrover.core.ActionExecutor;
import gov.nasa.marsrover.core.action.Action;
import gov.nasa.marsrover.core.action.GetMarsPhotos;
import gov.nasa.marsrover.core.model.Photo;

public class RoverPhotosRepository implements PhotosRepository{

    private final ActionExecutor actionExecutor;
    private List<Photo> photos = new ArrayList<>();

    public RoverPhotosRepository(ActionExecutor actionExecutor){
        this.actionExecutor = actionExecutor;
    }

    @Override
    public void fetchPhotos(final OnPhotosReceiveListener listener) {
        if (!photos.isEmpty()){
            notifyOnPhotosReceived(listener);
        }
        GetMarsPhotos action = GetMarsPhotos.create();
        action.setOnProcessListener(new Action.OnProcessListener<Photo>() {
            @Override
            public void onProcessFinished(List<Photo> elements) {
                setPhotos(elements);
                notifyOnPhotosReceived(listener);
            }
        });
        actionExecutor.submit(action);
    }

    @Override
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    private void notifyOnPhotosReceived(OnPhotosReceiveListener listener) {
        if (listener != null) {
            listener.onPhotosReceived(photos);
        }
    }
}
