package gov.nasa.marsrover.core.action;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import gov.nasa.marsrover.core.CoreException;
import gov.nasa.marsrover.core.parser.IParser;


public abstract class BaseHttpAction<T> implements Action<T> {

    protected final String uri;
    protected final IParser<T> parser;
    protected OnExecuteListener onExecuteListener;
    protected OnProcessListener<T> onProcessListener;
    protected List<T> elements = new ArrayList<>();

    public BaseHttpAction(String uri, IParser<T> parser) {
        this.uri = uri;
        this.parser = parser;
    }

    @Override
    public int getConnectionTimeout() {
        return DEFAULT_CONNECTION_TIMEOUT;
    }


    @Override
    public void setOnExecuteListener(OnExecuteListener listener) {
        this.onExecuteListener = listener;
    }

    @Override
    public void setOnProcessListener(OnProcessListener<T> listener) {
        this.onProcessListener = listener;
    }

    @Override
    public void onExecute() {
        try {
            executeAction();
        } catch (IOException e) {
            notifyOnExecutionError(new CoreException(CoreException.ErrorCode.GENERAL_ERROR));
            e.printStackTrace();
        }
    }

    protected void executeAction() throws IOException {
        InputStream stream = null;
        HttpURLConnection httpURLConnection = null;
        String result;
        try {
            httpURLConnection = buildHttpURLConnection();
            httpURLConnection.connect();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                notifyOnExecutionError(new CoreException(CoreException.ErrorCode.HTTP_URL_CONNECTION_ERROR));
            }
            stream = httpURLConnection.getInputStream();
            result = readStream(stream);
            onProcessResponse(result);
        } finally {
            if (stream != null) {
                stream.close();
            }

            if (httpURLConnection != null ){
                httpURLConnection.disconnect();
            }
        }
    }

    protected HttpURLConnection buildHttpURLConnection() throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) getUrl().openConnection();
        httpURLConnection.setConnectTimeout(getConnectionTimeout());
        httpURLConnection.setRequestMethod(getMethod().getRequestMethod());
        return httpURLConnection;
    }

    private String readStream(InputStream stream) throws IOException {
        int character;
        Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        StringBuilder buffer = new StringBuilder();
        while((character = reader.read()) != -1)
            buffer.append((char) character);
        return buffer.toString();
    }

    protected void notifyOnExecutionError(CoreException e) {
        if (onExecuteListener != null) {
            onExecuteListener.onExecuteError(this, e);
        }
    }

    protected void notifyOnExecutionSuccess() {
        if (onExecuteListener != null) {
            onExecuteListener.onExecuteSuccess(this);
        }
    }

    protected void notifyOnProcessFinished() {
        if (onProcessListener != null) {
            onProcessListener.onProcessFinished(elements);
        }
    }
}
