package gov.nasa.marsrover.core.model;


public class DefaultValues {

    public static final long DEFAULT_ID = 0;
    public static final String DEFAULT_STRING = "";
    public static final long DEFAULT_LONG = 0;
    public static final int DEFAULT_INT = 0;
}
