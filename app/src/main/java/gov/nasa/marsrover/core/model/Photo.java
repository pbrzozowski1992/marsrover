package gov.nasa.marsrover.core.model;


public class Photo extends BaseModel {

    public static final String SOL = "sol";
    public static final String IMAGE_SRC = "img_src";
    public static final String EARTH_DATE = "earth_date";
    public static final String CAMERA = "camera";
    public static final String ROVER = "rover";

    private long sol = DefaultValues.DEFAULT_LONG;
    private String imgSrc = DefaultValues.DEFAULT_STRING;
    private String earthDate = DefaultValues.DEFAULT_STRING;
    private Camera camera = new Camera(DefaultValues.DEFAULT_ID);
    private Rover rover = new Rover(DefaultValues.DEFAULT_ID);


    public Photo(long id, long sol, String imgSrc, String earthDate, Camera camera, Rover rover) {
        super(id);
        this.sol = sol;
        this.imgSrc = imgSrc;
        this.earthDate = earthDate;
        this.camera = camera;
        this.rover = rover;
    }

    public long getSol() {
        return sol;
    }

    public void setSol(long sol) {
        this.sol = sol;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getEarthDate() {
        return earthDate;
    }

    public void setEarthDate(String earthDate) {
        this.earthDate = earthDate;
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public Rover getRover() {
        return rover;
    }

    public void setRover(Rover rover) {
        this.rover = rover;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "sol=" + sol +
                ", imgSrc='" + imgSrc + '\'' +
                ", earthDate='" + earthDate + '\'' +
                ", camera=" + camera +
                ", rover=" + rover +
                '}';
    }
}
