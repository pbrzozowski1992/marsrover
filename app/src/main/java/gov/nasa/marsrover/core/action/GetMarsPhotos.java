package gov.nasa.marsrover.core.action;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import gov.nasa.marsrover.core.CoreException;
import gov.nasa.marsrover.core.model.RequestMethod;
import gov.nasa.marsrover.core.model.Photo;
import gov.nasa.marsrover.core.parser.IParser;
import gov.nasa.marsrover.core.parser.PhotoParser;
import gov.nasa.marsrover.utils.Log;

public class GetMarsPhotos extends BaseHttpAction<Photo> {

    private static final String TAG = GetMarsPhotos.class.getSimpleName();
    private static final String PHOTOS_FIELD = "photos";
    private static final String REQUEST_URI =
            "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&api_key=7mta2DvwjHp5uM2kauLJVs8WZELye16YelClOU2J";

    public static GetMarsPhotos create() {
        return new GetMarsPhotos(REQUEST_URI, new PhotoParser());
    }

    private GetMarsPhotos(String uri, IParser<Photo> parser) {
        super(uri, parser);
    }

    @Override
    public void onSubmit() {
        Log.d(TAG, "onSubmit");
    }

    @Override
    public void onExecute() {
        super.onExecute();
        Log.d(TAG, "onExecute");
    }

    @Override
    public void onPostExecute() {
        Log.d(TAG, "onPostExecute");
        notifyOnProcessFinished();
    }

    @Override
    public void onProcessResponse(String response) {
        Log.d(TAG, "onProcessResponse: " + response);
        try {
            JSONObject responseJSONObject = new JSONObject(response);
            JSONArray responseJSONArray = responseJSONObject.optJSONArray(PHOTOS_FIELD);
            for (int arrayIndex = 0; arrayIndex<responseJSONArray.length(); arrayIndex++) {
                JSONObject jsonObject = responseJSONArray.getJSONObject(arrayIndex);
                elements.add(parser.parseFromJsonObject(jsonObject));
            }
            notifyOnExecutionSuccess();
        } catch (JSONException e) {
            notifyOnExecutionError(new CoreException(CoreException.ErrorCode.INVALID_RESPONSE));
            e.printStackTrace();
        }
    }

    @Override
    public void complete() {
        
    }

    @Override
    public RequestMethod getMethod() {
        return RequestMethod.GET;
    }

    @Override
    public URL getUrl() throws MalformedURLException {
        return URI.create(uri).toURL();
    }

}
