package gov.nasa.marsrover.core.model;


public class Camera extends BaseModel {

    public static final String NAME = "name";
    public static final String ROVER_ID = "rover_id";
    public static final String FULL_NAME = "full_name";

    private String name = DefaultValues.DEFAULT_STRING;
    private long roverId = DefaultValues.DEFAULT_ID;
    private String fullName = DefaultValues.DEFAULT_STRING;

    public Camera(long id) {
        super(id);
    }

    public Camera(long id, String name, long roverId, String fullName) {
        super(id);
        this.name = name;
        this.roverId = roverId;
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getRoverId() {
        return roverId;
    }

    public void setRoverId(long roverId) {
        this.roverId = roverId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "Camera{" +
                "name='" + name + '\'' +
                ", roverId=" + roverId +
                ", fullName='" + fullName + '\'' +
                '}';
    }
}
