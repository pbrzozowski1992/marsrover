package gov.nasa.marsrover.core.model;


import java.util.ArrayList;
import java.util.List;

public class Rover extends BaseModel {

    public static final String NAME = "name";
    public static final String LANDING_DATE = "landing_date";
    public static final String LAUNCH_DATE = "launch_date";
    public static final String STATUS = "status";
    public static final String MAX_SOL = "max_sol";
    public static final String MAX_DATE = "max_date";
    public static final String TOTAL_PHOTOS = "total_photos";
    public static final String CAMERAS = "cameras";

    private String name = DefaultValues.DEFAULT_STRING;
    private String landingDate = DefaultValues.DEFAULT_STRING;
    private String launchDate = DefaultValues.DEFAULT_STRING;
    private String status = DefaultValues.DEFAULT_STRING;
    private int maxSol = DefaultValues.DEFAULT_INT;
    private String maxDate = DefaultValues.DEFAULT_STRING;
    private int totalPhotos = DefaultValues.DEFAULT_INT;
    private List<Camera> cameras = new ArrayList<>();

    public Rover(long id){
        super(id);
    }

    public Rover(long id, String name, String landingDate, String launchDate, String status,
                 int maxSol, String maxDate, int totalPhotos, List<Camera> cameras) {
        super(id);
        this.name = name;
        this.landingDate = landingDate;
        this.launchDate = launchDate;
        this.status = status;
        this.maxSol = maxSol;
        this.maxDate = maxDate;
        this.totalPhotos = totalPhotos;
        this.cameras = cameras;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLandingDate() {
        return landingDate;
    }

    public void setLandingDate(String landingDate) {
        this.landingDate = landingDate;
    }

    public String getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(String launchDate) {
        this.launchDate = launchDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMaxSol() {
        return maxSol;
    }

    public void setMaxSol(int maxSol) {
        this.maxSol = maxSol;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public int getTotalPhotos() {
        return totalPhotos;
    }

    public void setTotalPhotos(int totalPhotos) {
        this.totalPhotos = totalPhotos;
    }

    public List<Camera> getCameras() {
        return cameras;
    }

    public void setCameras(List<Camera> cameras) {
        this.cameras = cameras;
    }

    @Override
    public String toString() {
        return "Rover{" +
                "name='" + name + '\'' +
                ", landingDate='" + landingDate + '\'' +
                ", launchDate='" + launchDate + '\'' +
                ", status='" + status + '\'' +
                ", maxSol=" + maxSol +
                ", maxDate='" + maxDate + '\'' +
                ", totalPhotos=" + totalPhotos +
                ", cameras=" + cameras +
                '}';
    }
}
