package gov.nasa.marsrover.utils;


public interface Logger {
    /** Sends debug log message. */
    void d(String tag, String msg);

    /** Sends debug log message and logs the exception. */
    void d(String tag, String msg, Throwable tr);

    /** Sends error log message. */
    void e(String tag, String msg);

    /** Sends error log message and logs the exception. */
    void e(String tag, String msg, Throwable tr);

    /** Sends info log message. */
    void i(String tag, String msg);

    /** Sends info log message and logs the exception. */
    void i(String tag, String msg, Throwable tr);

    /** Sends verbose log message and logs the exception. */
    void v(String tag, String msg, Throwable tr);

    /** Sends verbose log message. */
    void v(String tag, String msg);

    /** Sends warning log message and logs the exception. */
    void w(String tag, String msg, Throwable tr);

    /** Sends warning log message. */
    void w(String tag, String msg);
}
