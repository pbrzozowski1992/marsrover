package gov.nasa.marsrover.utils;

public class Log {

    public static Logger logger = new Logger() {
        @Override
        public void d(String tag, String msg) {
            android.util.Log.d(tag, msg);
        }

        @Override
        public void d(String tag, String msg, Throwable tr) {
            android.util.Log.d(tag, msg, tr);
        }

        @Override
        public void e(String tag, String msg) {
            android.util.Log.e(tag, msg);
        }

        @Override
        public void e(String tag, String msg, Throwable tr) {
            android.util.Log.e(tag, msg, tr);
        }

        @Override
        public void i(String tag, String msg) {
            android.util.Log.i(tag, msg);
        }

        @Override
        public void i(String tag, String msg, Throwable tr) {
            android.util.Log.i(tag, msg, tr);
        }

        @Override
        public void v(String tag, String msg, Throwable tr) {
            android.util.Log.v(tag, msg, tr);
        }

        @Override
        public void v(String tag, String msg) {
            android.util.Log.v(tag, msg);
        }

        @Override
        public void w(String tag, String msg, Throwable tr) {
            android.util.Log.w(tag, msg, tr);
        }

        @Override
        public void w(String tag, String msg) {
            android.util.Log.w(tag, msg);
        }
    };

    public static void d(String tag, String msg) {
        logger.d(tag, msg);
    }

    public static void d(String tag, String msg, Throwable tr) {
        logger.d(tag, msg, tr);
    }

    public static void e(String tag, String msg) {
        logger.e(tag, msg);
    }

    public static void e(String tag, String msg, Throwable tr) {
        logger.e(tag, msg, tr);
    }

    public static void i(String tag, String msg) {
        logger.i(tag, msg);
    }

    public static void i(String tag, String msg, Throwable tr) {
        logger.i(tag, msg, tr);
    }

    public static void v(String tag, String msg, Throwable tr) {
        logger.v(tag, msg, tr);
    }

    public static void v(String tag, String msg) {
        logger.v(tag, msg);
    }

    public static void w(String tag, String msg, Throwable tr) {
        logger.w(tag, msg, tr);
    }

    public static void w(String tag, String msg) {
        logger.w(tag, msg);
    }
}
